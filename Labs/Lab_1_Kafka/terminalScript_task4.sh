#!/bin/bash

# wait 2 seconds to start the kafka broker after Zookeeper was started in the previous tab,
# wait 5 seconds to create the wikimedia topic,
# wait 7 seconds to start the default console consumer,
# you can use the last tabs to launch the Proxy, the Filters, and the Aggregator

# trailing /bin/bash is to provide a bash interpreter after the previous command is finished

# HISTFILE is to add commands to the history (then it's easy to re-launch one of them using the upper key in one of the tabs)
# remove write permission on the history file if you don't want it modified by your actions

HISTFILE=./history_task4 xfce4-terminal --tab --title=Zookeeper --hold --working-directory=./ --command="/bin/bash -c 'echo GOGO ; $KAFKA_HOME/bin/zookeeper-server-start.sh $KAFKA_HOME/config/zookeeper.properties'" --tab --title=KafkaBroker --hold --working-directory=./ --command="/bin/bash -c 'sleep 2s ; $KAFKA_HOME/bin/kafka-server-start.sh $KAFKA_HOME/config/server.properties ; /bin/bash'" --tab --title=Topic --hold --working-directory=./ --command="/bin/bash -c 'sleep 5s ; $KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic wikimedia ; $KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic region ;/bin/bash'" --tab --title=WikimediaConsumer --hold --working-directory=./ --command="/bin/bash -c 'sleep 7s ; $KAFKA_HOME/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic wikimedia --from-beginning ; /bin/bash'" --tab --title=RegionConsumer --hold --working-directory=./ --command="/bin/bash -c 'sleep 7s ; $KAFKA_HOME/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic region --from-beginning ; /bin/bash'" --tab --title=Proxy --hold --working-directory=./ --command="/bin/bash -c ' sleep 7s ; /bin/bash'" --tab --title=Filter1 --hold --working-directory=./ --command="/bin/bash -c ' sleep 7s ; /bin/bash'"  --tab --title=Filter2 --hold --working-directory=./ --command="/bin/bash -c ' sleep 7s ; /bin/bash'"  --tab --title=Aggregator --hold --working-directory=./ --command="/bin/bash -c ' sleep 7s ; /bin/bash'"


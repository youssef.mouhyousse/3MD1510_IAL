package proxy;

import java.net.URI;
import java.util.concurrent.TimeUnit;

import com.launchdarkly.eventsource.EventHandler;
import com.launchdarkly.eventsource.EventSource;
import com.launchdarkly.eventsource.MessageEvent;

public class ProxyThatPrints implements EventHandler {

	public static void main(String[] args) {
		new ProxyThatPrints();
	}

	ProxyThatPrints() {
		String url = "https://stream.wikimedia.org/v2/stream/recentchange";
		EventSource.Builder builder = new EventSource.Builder(this, URI.create(url));
		EventSource eventSource = builder.build();
		eventSource.start();

		// listen to the stream for 5 seconds
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// listen to the stream for ever
		/*
		 * while (true) { // empty on purpose }
		 */
	}

	public void onOpen() throws Exception {
		System.out.println("The stream connection has been opened.");
	}

	public void onClosed() throws Exception {
		System.out.println("The stream connection has been closed.");
	}

	public void onMessage(String event, MessageEvent messageEvent) throws Exception {
		String message = messageEvent.getData();
		System.out.println(message);
	}

	public void onComment(String comment) throws Exception {
		System.out.println("A comment line (any line starting with a colon) was received from the stream: " + comment);
	}

	public void onError(Throwable t) {
		System.out.println("An exception occured on the socket connection: " + t.getMessage());
	}
}
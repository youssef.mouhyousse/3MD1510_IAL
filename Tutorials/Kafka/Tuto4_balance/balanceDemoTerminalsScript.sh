#!/bin/bash

# This is an example on how to launch a terminal with multiple tabs, give each tab a title and execute a command in each tab (Zookeeper, Kafka broker, topics creation, kafka consumer, kafka producer).
# --hold is to keep the tab open after the command is finished.
# --working-directory
# --command

# wait 2 seconds to start the kafka broker after Zookeeper was started in the previous tab,
# wait 5 seconds to create the test topic with 3 partitions
# wait 10 seconds to start a custom producer, a custom consumer
# 3 additionnal tabs are ready to start new consumers

# trailing /bin/bash is to provide a bash interpreter after the previous command is finished

# HISTFILE is to add the 6 commands to the history (then it's easy to re-launch one of them using the upper key in one of the tabs)
# remove write permission on the history file if you don't want it modified by your actions

HISTFILE=./history xfce4-terminal --tab --title=Zookeeper --hold --working-directory=./ --command="/bin/bash -c 'echo GOGO ; $KAFKA_HOME/bin/zookeeper-server-start.sh $KAFKA_HOME/config/zookeeper.properties'" --tab --title=KafkaBroker --hold --working-directory=./ --command="/bin/bash -c 'sleep 2s ; $KAFKA_HOME/bin/kafka-server-start.sh $KAFKA_HOME/config/server.properties ; /bin/bash'" --tab --title=Topic --hold --working-directory=./ --command="/bin/bash -c 'sleep 5s ; $KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 3 --topic test ; /bin/bash'" --tab --title=Consumer1 --hold --working-directory=./ --command="/bin/bash -c 'sleep 10s ; java -jar kafka_tuto_4/target/MyVerboseConsumer-jar-with-dependencies.jar ; /bin/bash'" --tab --title=Producer --hold --working-directory=./ --command="/bin/bash -c ' sleep 10s ; java -jar kafka_tuto_4/target/MyVerboseProducer-jar-with-dependencies.jar ; /bin/bash'" --tab --title=Consumer2 --hold --working-directory=./ --command="/bin/bash -c 'sleep 10s ; /bin/bash'" --tab --title=Consumer3 --hold --working-directory=./ --command="/bin/bash -c 'sleep 10s ; /bin/bash'" --tab --title=Consumer4 --hold --working-directory=./ --command="/bin/bash -c 'sleep 10s ; /bin/bash'"


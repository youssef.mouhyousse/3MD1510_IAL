#!/bin/bash

# This is an example on how to launch a terminal with multiple tabs, give each tab a title and execute a command in each tab (Zookeeper, Kafka broker, topic creation, kafka consumer, kafka producer).
# --hold is to keep the tab open after the command is finished.
# --working-directory
# --command

# wait 2 seconds to start a kafka broker after Zookeeper was started in the previous tab,
# wait 5 seconds to create the test topic
# wait 15 seconds to start 2 additionnal kafka brokers
# wait 25 seconds to create the my-replicated-topic topic
# wait 35 seconds to start the default console consumer and producer

# trailing /bin/bash is to provide a bash interpreter after the previous command is finished

# HISTFILE is to add the various commands to the history (then it's easy to re-launch one of them using the upper key in one of the tabs)
# remove write permission on the history file if you don't want it modified by your actions

HISTFILE=./history xfce4-terminal --tab --title=Zookeeper --hold --working-directory=./ --command="/bin/bash -c 'echo GOGO ; $KAFKA_HOME/bin/zookeeper-server-start.sh $KAFKA_HOME/config/zookeeper.properties'" --tab --title=KafkaBroker1 --hold --working-directory=./ --command="/bin/bash -c 'sleep 2s ; $KAFKA_HOME/bin/kafka-server-start.sh $KAFKA_HOME/config/server.properties ; /bin/bash'" --tab --title=TopicTest --hold --working-directory=./ --command="/bin/bash -c 'sleep 5s ; $KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic test ; /bin/bash'" --tab --title=KafkaBroker2 --hold --working-directory=./ --command="/bin/bash -c 'sleep 15s ; $KAFKA_HOME/bin/kafka-server-start.sh ./kafka_tuto_5/server-1.properties ; /bin/bash'" --tab --title=KafkaBroker2 --hold --working-directory=./ --command="/bin/bash -c 'sleep 15s ; $KAFKA_HOME/bin/kafka-server-start.sh ./kafka_tuto_5/server-2.properties ; /bin/bash'" --tab --title=TopicReplicated --hold --working-directory=./ --command="/bin/bash -c 'sleep 25s ; $KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 3 --partitions 1 --topic my-replicated-topic ; /bin/bash'" --tab --title=Consumer --hold --working-directory=./ --command="/bin/bash -c 'sleep 35s ; $KAFKA_HOME/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic my-replicated-topic --from-beginning --consumer-property group.id=testconsumers ; /bin/bash'" --tab --title=Producer --hold --working-directory=./ --command="/bin/bash -c ' sleep 35s ; $KAFKA_HOME/bin/kafka-console-producer.sh --bootstrap-server localhost:9092 --topic my-replicated-topic ; /bin/bash'"


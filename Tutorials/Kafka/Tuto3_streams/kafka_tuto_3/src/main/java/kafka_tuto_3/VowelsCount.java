package kafka_tuto_3;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KGroupedStream;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.apache.kafka.streams.kstream.Predicate;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.ValueMapper;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.kstream.ValueTransformerSupplier;
import org.apache.kafka.streams.processor.ProcessorContext;

public class VowelsCount {
	public static void main(final String[] args) {
		new VowelsCount();
	}

	public VowelsCount() {
		Topology vowelsCountTopology = createVowelsCountTopology();
		KafkaStreams vowelsCountStream = new KafkaStreams(vowelsCountTopology, configureVowelsCount());
		vowelsCountStream.start();
	}

	private Topology createVowelsCountTopology() {
		StreamsBuilder builder = new StreamsBuilder();

		// creates a KStream from the input-topic
		KStream<String, String> source = builder.stream("input-topic");

		// materializes this KStream to the output-topic
		//source.to("output-topic");

		// creates a KStream with upper-case records
		ValueTransformerSupplier<String, String> upperCaseValueTransformerSupplier = new ValueTransformerSupplier<String, String>() {
			public ValueTransformer<String, String> get() {
				return new ValueTransformer<String, String>() {
					public void init(ProcessorContext context) {
						// empty on purpose
					}
					public String transform(String value) {
						return value.toUpperCase();
					}
					public void close() {
						// empty on purpose					
					}
				};
			}
		};
		KStream<String, String> upperCaseStream = source.transformValues(upperCaseValueTransformerSupplier);
		//upperCaseStream.to("output-topic");

		// creates a KStream of characters
		ValueMapper<String, List<String>> letterValueMapper = new ValueMapper<String, List<String>>() {
			public List<String> apply(String value) {
				String[] liste =value.split("(?<=\\G.{1})");
				return Arrays.asList(liste);
			}
		};
		KStream<String, String> charactersStream = upperCaseStream.flatMapValues(letterValueMapper);
		//charactersStream.to("output-topic");  

		// keeps only the vowels
		Predicate<String, String> vowelPredicate = new Predicate<String, String>() {
			public boolean test(String key, String value) {
				return "AEIOUY".contains(value);
			}
		};
		KStream<String, String> vowelsStream = charactersStream.filter(vowelPredicate);
		//vowelsStream.to("output-topic");

		// GROUP AND COUNT
		// First, we group records by value (which contains the letter):
		KeyValueMapper<String, String, String> selector = new KeyValueMapper<String, String, String>() {
			public String apply(String key, String value) {
				return value;
			}
		};
		KGroupedStream<String, String> vowelsGroupedStream = vowelsStream.groupBy(selector);

		// Second, we feed a KTable which holds the count for each group:
		KTable<String, Long> vowelsCountTable = vowelsGroupedStream.count();
		// Each time the table gets updated, we want the updated row to be sent to a new KStream:
		KStream<String, Long> vowelsCountingStreams = vowelsCountTable.toStream();
		// The types of the records in this KStream are 
		//     String for the key (since the key is now the letter, because of our selector for the groupBy operation)
		//     and Long for the value (since the value is the number of occurrences of the letter).
		// When we want to materialize the record back to the output-topic, we need to specify the new Serde:
		vowelsCountingStreams.to("output-topic", Produced.with(Serdes.String(), Serdes.Long()));

		return builder.build();
	}

	private Properties configureVowelsCount() {
		Properties vowelsCountStreamsProperties = new Properties();
		vowelsCountStreamsProperties.put(StreamsConfig.APPLICATION_ID_CONFIG, "vowelsCount");
		vowelsCountStreamsProperties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		// The semantics of caching is that data is flushed to the state store and
		// forwarded to the next downstream processor node whenever the earliest of
		// commit.interval.ms or cache.max.bytes.buffering (cache pressure) hits.
		vowelsCountStreamsProperties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
		vowelsCountStreamsProperties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG,
				Serdes.String().getClass().getName());
		vowelsCountStreamsProperties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG,
				Serdes.String().getClass().getName());
		vowelsCountStreamsProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		return vowelsCountStreamsProperties;
	}
}
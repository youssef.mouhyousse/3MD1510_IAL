#!/bin/bash

# This is an example on how to launch a terminal with multiple tabs, give each tab a title and execute a command in each tab (Zookeeper, Kafka broker, topics creation, kafka consumer, kafka producer).
# --hold is to keep the tab open after the command is finished.
# --working-directory
# --command

# wait 2 seconds to start the kafka broker after Zookeeper was started in the previous tab,
# wait 5 seconds to create the intput-topic and output-topic topics
# wait 10 seconds to start the default console producer and the console consumer with print.key and print.value set to true

# trailing /bin/bash is to provide a bash interpreter after the previous command is finished

# HISTFILE is to add the 6 commands to the history (then it's easy to re-launch one of them using the upper key in one of the tabs)
# remove write permission on the history file if you don't want it modified by your actions

HISTFILE=./history xfce4-terminal --tab --title=Zookeeper --hold --working-directory=./ --command="/bin/bash -c 'echo GOGO ; $KAFKA_HOME/bin/zookeeper-server-start.sh $KAFKA_HOME/config/zookeeper.properties'" --tab --title=KafkaBroker --hold --working-directory=./ --command="/bin/bash -c 'sleep 2s ; $KAFKA_HOME/bin/kafka-server-start.sh $KAFKA_HOME/config/server.properties ; /bin/bash'" --tab --title=Topics --hold --working-directory=./ --command="/bin/bash -c 'sleep 5s ; $KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --topic input-topic ; $KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --topic output-topic ; /bin/bash'" --tab --title=Producer --hold --working-directory=./ --command="/bin/bash -c 'sleep 10s ; $KAFKA_HOME/bin/kafka-console-producer.sh --bootstrap-server localhost:9092 --property parse.key=true --property key.separator="*" --topic input-topic ; /bin/bash'" --tab --title=Consumer --hold --working-directory=./ --command="/bin/bash -c ' sleep 10s ; $KAFKA_HOME/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic output-topic --from-beginning --property print.key=true --property print.value=true ; /bin/bash'"


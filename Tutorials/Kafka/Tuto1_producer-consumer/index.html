<html>
<body>
<h1>Consumer and Producer APIs</h1>

<p>In this tutorial, we proceed by steps:</p>
<ol>
    <li>Start from the "Hello World!" scenario:
        <ol>
            <li>Use existing scripts to start a Kafka broker and create a topic "<i>test</i>".</li>
            <li>Start a command-line/console producer which reads strings from the standard input and posts them to the topic.</li>
            <li>Start a command-line/console consumer which reads messages from the topic and prints them to the standard output / screen.</li>
        </ol>
    </li>
    <li><a href="#consumer">Write a Java application acting as an additional consumer.</a></li>
    <li><a href="#producer">Write a Java application acting as an additional producer.</a></li>
</ol>

<p>Note: <a href="#gitlab">At the bottom</a> of most tutorial pages of this series you will find a link to the GitLab project hosting the full source code.</p>

<h2>Starting point: run the "Hello World!" scenario with the Console Producer and Console Consumer</h2>
<ol>
    <li>Check to see if the Zookeeper daemon is running on your machine, and if not, start the service using the convenience script to get a single-node ZooKeeper instance:<br>
        <code>$KAFKA_HOME/bin/zookeeper-server-start.sh $KAFKA_HOME/config/zookeeper.properties &amp;</code></li>
    <li>Start the Kafka broker with the default configuration:<br>
        <code>$KAFKA_HOME/bin/kafka-server-start.sh $KAFKA_HOME/config/server.properties &amp;</code></li>
    <li>Create a topic named "<i>test</i>" with a single partition:<br>
        <code>$KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 1 --topic test</code><br>
        If a topic "<i>test</i>" already exists from your previously experiments, delete it before re-creating it:<br>
        <code>$KAFKA_HOME/bin/kafka-topics.sh --delete --bootstrap-server localhost:9092 --topic test</code></li>
    <li>Send some messages using Kafka console producer:<br>
        <code>$KAFKA_HOME/bin/kafka-console-producer.sh --bootstrap-server localhost:9092 --topic test</code><br>
        <pre>&gt;Hello world!</pre></li>
    <li>In another terminal, start Kafka command-line consumer:<br>
        <code>$KAFKA_HOME/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --from-beginning --consumer-property group.id=testconsumers</code><br>
        <strong><span class="" style="color: rgb(239, 69, 64);">Note the additional option "</span><code><span class="" style="color: rgb(239, 69, 64);">group.id</span></code><span class="" style="color: rgb(239, 69, 64);">" which was not used in the "Hello World!" page.</span></strong> More about that in <a href="#consumer">the Consumer section</a> shortly.<br>
        You should see the line input previously ("<i>Hello world!</i>").</li>
</ol>


<p>All these operations are automated in the <a href="https://galtier.pages.centralesupelec.fr/3MD1510_IAL/Tutorials/Kafka/Tuto0_HelloWorld/prodConsoTerminalsExampleScript.sh">the <code>prodConsoTerminalsExampleScript.sh</code> script</a> that launchs a terminal with multiple titled tabs, and run each command in a tab; you will also need to download <a href="https://galtier.pages.centralesupelec.fr/3MD1510_IAL/Tutorials/Kafka/Tuto0_HelloWorld/history">the <code>history</code></a> file (see the comments at the beginning of the script). To set the execution permission on the script use <code>chmod +x prodConsoTerminalsExampleScript.sh</code><br>
This script requires xfce4-terminal. It's already installed in the container. To install it in the VM, use <code>sudo apt-get install -y xfce4-terminal</code></p>



<a name="consumer">
    <h2>Code a Consumer</h2>
</a>

<p>Start Eclipse, create a new Maven simple project "<i>kafka_tuto_1</i>".</p>

<p>Our code will use the Kafka library. The <a href="https://kafka.apache.org/documentation/#consumerapi">Kafka documentation for the Consumer API</a> indicates the dependency we need to declare in our project in order for Maven to retreive the appropriate
    files. Add the following code to your <code>pom.xml</code> file in the <i>dependencies</i> section:</p>
<textarea rows="6" cols="100">&lt;!-- https://mvnrepository.com/artifact/org.apache.kafka/kafka-clients --&gt;
&lt;dependency&gt;
    &lt;groupId&gt;org.apache.kafka&lt;/groupId&gt;
    &lt;artifactId&gt;kafka-clients&lt;/artifactId&gt;
    &lt;version&gt;3.2.1&lt;/version&gt;
&lt;/dependency&gt;</textarea>

<p>Also add the following to the project config:</p>
<textarea rows="4" cols="100">	<properties>
		<maven.compiler.source>1.8</maven.compiler.source>
		<maven.compiler.target>1.8</maven.compiler.target>
	</properties></textarea>


<h3>Configure the Kafka Consumer</h3>

<p>Create a new class "<i>MyConsumer</i>".</p>

<p>The <a href="https://kafka.apache.org/32/javadoc/org/apache/kafka/clients/consumer/KafkaConsumer.html">KafkaConsumer &lt;K,V&gt;</a> type is parameterized with the type of the key and the type of the value of records handled by the consumer. <br></p>
<p>To create an instance, we need to provide a set of <a href="https://docs.oracle.com/javase/8/docs/api/java/util/Properties.html?is-external=true">Properties</a> as configuration.</p>

<p>The first one is mandatory, no matter the programming language: it's a list of "<strong><i>bootstrap servers</i></strong>". This list of <code>address[:port]</code> of Kafka servers is used by the client (consumer or producer) to discover the rest of
    the brokers in the cluster and need not be an exhaustive list of all servers in the cluster (though you may want to specify more than one in case there are servers down when the client is connecting). For this tutorial, the consumer runs on the same
    machine as the Kafka broker: <i>localhost</i>. And since we kept the default configuration when starting the broker, it uses the default port: <i>9092</i>.</p>

<p>Valid configuration strings are provided by <a href="https://kafka.apache.org/32/javadoc/org/apache/kafka/clients/consumer/ConsumerConfig.html">the ConsumerConfig class</a> (you can write directly "bootstrap.servers" in your code, but using the <code>ConsumerConfig</code>    class avoids misspelling errors and lets you benefit from the IDE auto-completion) and documented in the <a href="http://kafka.apache.org/documentation.html#consumerconfigs">Kafka documentation</a>.</p>

<p>The bootstrap servers list is given by the <code>BOOTSTRAP_SERVERS_CONFIG</code> property, as a comma-separated list of <i>host:port</i> pairs.</p>

<p>Another property we can specify is from which point in the partition we want to read messages (where to position our <i>offset</i>): the beginning (to process past events) or the latest records (if we're only interested in current events).</p>

<p>The reading position is given by the <code>AUTO_OFFSET_RESET_CONFIG</code> property; setting it to "<code>earliest</code>" means the consumer will start reading messages from the beginning of that topic.</p>

<p>Our consumer will subscribe to the "test" topic. If multiple consumers are part of the same group of subscribers to a given topic, when a new record is published to the topic only one of the members of the group gets it; if two groups of consumers subscribe
    to the same topic, when a new record is published one member in each group gets it. We'll get into more details about that in Tuto#04. For this tutorial, we want our consumer to be in a different group than the console consumer so that each of them
    displays all the messages. If you didn't set a <i>group id</i> when starting the command-line consumer, you can discover which id was automatically assigned by running <code>$KAFKA_HOME/bin/kafka-consumer-groups.sh --list --bootstrap-server localhost:9092</code>.
    Then you just have to specify a different value in the <i>group id</i> property for the Java consumer.</p>

<p>The consumer group id is given by the <code>GROUP_ID_CONFIG</code> property.</p>

<p>The Java library requires 2 additional properties: Kafka records are &lt;key, value&gt; pairs and are sent over the network as byte arrays; but to make the code more readable, the Kafka Java API allows, using parameterized types, any Java object to be
    sent as key and value; it means that the producer has to know how to convert these objects to ByteArrays. The <code>KEY_DESERIALIZER_CLASS_CONFIG</code> and <code>VALUE_DESERIALIZER_CLASS_CONFIG</code> properties indicate the serializers to use. We'll
    discuss serializers and deserializers in more detail in Tuto#02.</p>

<p>Resulting code snippet:</p>
<textarea rows="32" cols="120">package kafka_tuto_1;

import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;

public class MyConsumer {

	public static void main(String[] args) {
		new MyConsumer();
	}

	MyConsumer() {
		// records handled by this consumer will have no key, and a string value
		KafkaConsumer<Void, String> kafkaConsumer;
		// create the Kafka consumer with the appropriate configuration
		kafkaConsumer = new KafkaConsumer<>(configureKafkaConsumer());
	}

	private Properties configureKafkaConsumer() {
		Properties consumerProperties = new Properties();
		consumerProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.VoidDeserializer");
		consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringDeserializer");
		consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest"); // from beginning
		consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, "myOwnPrivateJavaGroup");
		return consumerProperties;
	}
}</textarea>



<h3>Subscribe to the topic</h3>

<p>The next step is to create the consumer and to subscribe to one or more topics by listing their names.</p>

<textarea rows="10" cols="120">[...]
import java.util.Collections;
[...]

	MyConsumer() {
                [...]
		// subscribes to the 'test' topic
		kafkaConsumer.subscribe(Collections.singletonList("test"));
  }
[...]</textarea>
<p>Note: It is also possible to call <i>subscribe</i> with the regular expression; when a new topic with the name that matches the expression is created, the consumer will almost immediately start consuming from it.</p>



<h3>Read records from the topic</h3>

<p>Kafka uses a <i>poll strategy</i> and not an observer pattern: the consumer is not automatically notified when new records appear in the topic it subscribed to; instead, the consumer needs to repeatedly fetch data from the topic, using a <i>poll</i> method/function.
    The poll method returns immediately if there are records available. Otherwise, it will await a given timeout. If the timeout expires, an empty record set will be returned.</p>
<p>Note: The poll method also serves as a heartbeat for the coordinator of a group to be aware of active members. We'll discuss consumer groups in Tuto#04. For that reason, it is a good practice to <code>close()</code> the consumer before exiting: it releases
    associated network resources and, in case the consumer is part of a group, it allows its coordinator to be immediately aware rather than waiting for no-longer coming in heartbeats.</p>
<p>The poll retrieves records at a given index in the partition, an "<i>offset</i>" that Kafka maintains to uniquely identify each record in the partition. On each poll, the consumer will usually try to use its last consumed offset as the starting offset
    and fetch sequentially. But the consumer can also specify a lower offset, to reprocess past events for instance.</p>

<p>The <a href="https://kafka.apache.org/32/javadoc/org/apache/kafka/clients/consumer/KafkaConsumer.html#poll-java.time.Duration-">poll</a> method returns a set of records, which might be empty if no records were published since the previous call.</p>
<textarea rows="22" cols="100">[...]
import java.time.Duration;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
[...]
		kafkaConsumer.subscribe(Collections.singletonList("test"));
		// reads from the topic
		try {
			Duration timeout = Duration.ofMillis(1000);
			ConsumerRecords&lt;Void, String&gt; records = null;
			while (true) { // I'm a machine, I can work forever :-)
				records = kafkaConsumer.poll(timeout);
				for (ConsumerRecord&lt;Void, String&gt; record : records)
					System.out.println(record.value());
			}
		} catch (Exception e) {
			System.err.println("something went wrong... " + e.getMessage());
		} finally {
			kafkaConsumer.close();
		}
[...]</textarea>


<h3>Run the Consumer</h3>

<p>Run the consumer application. You should see the record already published using the console producer and already displayed by the console consumer. Type a new line in the command-line producer terminal and check that both consumers get the new record.</p>

<h4>Option 1: Run the consumer application directly from Eclipse</h4>

<p>Note: You can ignore the message you get at the beginning:<br>
</p>
<pre><span class="" style="color: rgb(152, 202, 62);">SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.</span></pre>
<p></p>

<h4>Option 2: Build a JAR file and run it from the command line</h4>
<p>You will need that later, when we'll script the execution of the application.</p>
<p><u>Step 1: change the pom.xml file</u>:<br> If you want to be able to execute your application on a host where the Kafka library is absent, you can ask Maven to build a package (jar file) that will contain not only your compiled code but also all the
    required dependencies, such as Kafka jar files. Add the following lines to your <code>pom.xml</code> file in the <i>build / pluggins</i> section:</p>
<textarea rows="26" cols="120">			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
				<version>3.4.2</version>
				<configuration>
					<descriptorRefs>
						<descriptorRef>jar-with-dependencies</descriptorRef>
					</descriptorRefs>
				</configuration>
				<executions>
					<execution>
						<id>consumer</id>
						<phase>package</phase>
						<goals>
							<goal>single</goal>
						</goals>
						<configuration>
							<finalName>MyConsumer</finalName>
							<archive>
								<manifest>
									<mainClass>kafka_tuto_1.MyConsumer</mainClass>
								</manifest>
							</archive>
						</configuration>
					</execution>
				</executions>
			</plugin></textarea>

<p><u>Step 2: Build the jar file</u> including the necessary dependencies:</p>
<p>Using Eclipse you can right-click the <i>pom.xml</i> file, select "<i>Run As / Maven build</i>", enter "<i>clean package</i>" in the <i>Goals</i> field, and click <i>Run</i>. The jar file is generated in the <i>target</i> folder (press F5 if you don't
    see it).</p>
<p>Alternative: from the command-line :<code>mvn -f kafka_tuto_1/pom.xml clean package</code></p>

<p><u>Step 3: Run from the command line</u>:<br>
    <code>java -jar kafka_tuto_1/target/MyConsumer-jar-with-dependencies.jar</code></p>
<br>


<a name="producer">
    <h2>Code a Producer</h2>
</a>
<p>Add the following dependency to you <code>pom.xml</code> file:</p>
<textarea cols=150 rows=7>		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>2.0.2</version>
			<type>pom</type>
		</dependency>
</textarea>


<h3>Create and Configure the Kafka Producer</h3>

<p><a href="https://kafka.apache.org/32/javadoc/org/apache/kafka/clients/producer/KafkaProducer.html"><code>KafkaProducer</code></a> is parameterized with the key and value types of the records it will produce.</p>

<p>Similarly to the consumer, we provide a set of properties when creating the Kafka producer, including the list of bootstrap servers.</p>

<p>3 properties are mandatory:</p>
<ul>
    <li><code>BOOTSTRAP_SERVERS_CONFIG</code></li>
    <li><code>KEY_SERIALIZER_CLASS_CONFIG</code> and <code>VALUE_SERIALIZER_CLASS_CONFIG</code>: correspond to the KEY_DESERIALIZER_CLASS_CONFIG and VALUE_DESERIALIZER_CLASS_CONFIG of the consumer; the same way you set the class that will deserialize the byte array to the record key or value object, you set a class that will serialize the record key or value object to a byte array.</li>
</ul>
<p>Valid configuration strings for the <a href="https://docs.oracle.com/javase/8/docs/api/java/util/Properties.html?is-external=true">Properties</a> of a <a href="https://kafka.apache.org/32/javadoc/org/apache/kafka/clients/producer/KafkaProducer.html">KafkaProducer</a> are provided by <a href="https://kafka.apache.org/32/javadoc/org/apache/kafka/clients/producer/ProducerConfig.html">the ProducerConfig class</a> and documented at <a href="http://kafka.apache.org/documentation.html#producerconfigs">Kafka documentation</a>.</p>

<textarea rows="28" cols="100" spellcheck="false">package kafka_tuto_1;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;

public class MyProducer {
	public static void main(String[] args) {
		new MyProducer();
	}

	MyProducer() {
		// records sent by this producer will have no key, and a string value
		KafkaProducer<Void, String> kafkaProducer;
		// create the Kafka producer with the appropriate configuration
		kafkaProducer = new KafkaProducer<>(configureKafkaProducer());
	}

	private Properties configureKafkaProducer() {
		Properties producerProperties = new Properties();
		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.VoidSerializer");
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringSerializer");
		return producerProperties;
	}
}</textarea>


<h3>Write Records to the Kafka Topic</h3>

<p>The <a href="https://kafka.apache.org/32/javadoc/org/apache/kafka/clients/producer/ProducerRecord.html"><code>ProducerRecord</code></a> allows the producer to prepare a record. The <a href="https://kafka.apache.org/32/javadoc/org/apache/kafka/clients/producer/ProducerRecord.html#ProducerRecord-java.lang.String-K-V-">ProducerRecord constructor</a>    used below takes 3 arguments: the name of the topic to publish to, and the key and value of the record; the types of the key and value must be consistent with the Serializer classes set in the producer properties.</p>

<p>At last, use the <a href="https://kafka.apache.org/32/javadoc/org/apache/kafka/clients/producer/KafkaProducer.html#send-org.apache.kafka.clients.producer.ProducerRecord-">send method</a> to publish the record. In many production applications you wouldn't ignore the returned value as it can be used to know, for instance, whether the record was published successfully or not. But this simple "fire-and-forget" sending method is sufficent in our application where we don't really care if the record arrives successfully or not (most of the time it will since Kafka is highly available and will retry sending records automatically, however some records might still get lost). Two other methods are widely used:</p>
<ul>
    <li><i>Synchronous send:</i> the <code>send()</code> method returns a <code>Future</code> object and we use <code>get()</code> to block and wait on the future an see if the send was successful or not: <code>producer.send(record).get();</code></li>
    <li><i>Asynchronous send:</i> we provide a <i>callback</i> function to the send() method which gets triggered when the Kafka broker acknowledges the record (or signals an error): see pages 47-48 of "Kafka: The Definitive Guide" for a use case.</li>
</ul>
<textarea rows="28" cols="100">[...]
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.producer.ProducerRecord;

	MyProducer() {
		KafkaProducer&lt;Void, String&gt; kafkaProducer = new KafkaProducer&lt;&gt;(configureKafkaProducer());
		try {
			// create and sent records to the 'test' topic
			int i = 0;
			String message = null;
			while (true) {
				message = "{ \"number\": " + i++ + "}";
				kafkaProducer.send(new ProducerRecord&lt;Void, String&gt;("test", null, message));
				// wait 1 second
				try {
					TimeUnit.SECONDS.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			System.err.println("something went wrong... " + e.getMessage());
		} finally {
			kafkaProducer.close();
		}
	}
  [...]</textarea>

<h3>Run the Producer</h3>
<p>Run the producer (as you did for the consumer) and check that both consumers receive all the numbered messages.</p>

<a name="gitlab">
    <h2>Code</h2>
</a>
<p>You can get the full source code from the <a href="https://gitlab-research.centralesupelec.fr/galtier/3MD1510_IAL/-/tree/master/Tutorials/Kafka/Tuto1_producer-consumer">GitLab project</a>.</p>

</body>
</html>
